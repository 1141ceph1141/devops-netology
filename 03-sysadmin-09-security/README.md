# Домашнее задание к занятию "3.9. Элементы безопасности информационных систем"

1. Установите Bitwarden плагин для браузера. Зарегестрируйтесь и сохраните несколько паролей.

![bitwarden](.README_images/bitwarden.png)

![bitwarden_plugin](.README_images/bitwarden_plugin.png)

3. Установите Google authenticator на мобильный телефон. Настройте вход в Bitwarden акаунт через Google authenticator OTP.

![2fa](.README_images/2fa.png)

4. Установите apache2, сгенерируйте самоподписанный сертификат, настройте тестовый сайт для работы по HTTPS.

![apache2](.README_images/apache2.png)

![selfsigned](.README_images/selfsigned.png)

![apache_cert](.README_images/apache_cert.png)

5. Проверьте на TLS уязвимости произвольный сайт в интернете (кроме сайтов МВД, ФСБ, МинОбр, НацБанк, РосКосмос, РосАтом, РосНАНО и любых госкомпаний, объектов КИИ, ВПК ... и тому подобное).

![testssl](.README_images/testssl.png)

6. Установите на Ubuntu ssh сервер, сгенерируйте новый приватный ключ. Скопируйте свой публичный ключ на другой сервер. Подключитесь к серверу по SSH-ключу.
 
![ssh](.README_images/ssh.png)

7. Переименуйте файлы ключей из задания 5. Настройте файл конфигурации SSH клиента, так чтобы вход на удаленный сервер осуществлялся по имени сервера.

![ssh_centospc](.README_images/ssh_centospc.png)

9. Соберите дамп трафика утилитой tcpdump в формате pcap, 100 пакетов. Откройте файл pcap в Wireshark.

![tcpdump](.README_images/tcpdump.png)

![wireshark](.README_images/wireshark.png)