# Домашнее задание к занятию "5.1. Основы виртуализации"

## Задача 1

Опишите кратко, как вы поняли: в чем основное отличие полной (аппаратной) виртуализации, паравиртуализации и виртуализации на основе ОС.

### Ответ

*Полная (аппаратная) виртуализация как следует из названия полностью эмулирует физический компьютер с соответствующими физическими устройствами обеспечивая тотальную изоляцию гостевой ОС*

*При паравиртуализации используются гостевые операционные системы оптимизированные для работы с гипервизором, что позволяет увеличить эффективность утилизации аппаратных ресурсов. В отличие от полной виртуализации в данной технологии допускается прямое использование гостевой ОС аппаратных ресурсов хоста*

*Виртуализация на уровне ОС реализуется без отдельного слоя гипервизора, виртуализируется пользовательское окружение ОС.*

## Задача 2

Выберите тип один из вариантов использования организации физических серверов, в зависимости от условий использования.

Организация серверов:
- физические сервера
- паравиртуализация
- виртуализация уровня ОС

Условия использования:

- Высоконагруженная база данных, чувствительная к отказу
- Различные web-приложения
- Windows системы для использования Бухгалтерским отделом 
- Системы, выполняющие высокопроизводительные расчеты на GPU

Опишите, почему вы выбрали к каждому целевому использованию такую организацию.

### Ответ

- Высоконагруженная база данных, чувствительная к отказу - для размещения может использоваться как физический сервер (т.к. виртуализация снижает производительность в части утилизации I/O) так и [оптимизированная](https://www.vmware.com/content/dam/digitalmarketing/vmware/en/pdf/solutions/sql-server-on-vmware-best-practices-guide.pdf) паравиртуализированная инфраструктура обеспечивающая преимущества виртуализации (кластеризация, живая миграция по типу vMotion и т.д.) ценой увеличения мощности и соответственно стоимости аппаратных ресурсов.

- Различные web-приложения - в данном случае по моему мнению лучше всего подойдёт виртуализация уровня ОС, которая даёт преимущества при разработке, тестировании и размещении приложений.

- Windows системы для использования Бухгалтерским отделом - мне кажется, что такие преимущества паравиртуализации, как облегчение миграции и бекапирования, сделают её оптимальным выбором в этом случае. Также стоит отметить, что посредством паравиртуализации можно обеспечить поддержку legacy операционных систем на современном железе, что, конечно, не совсем правильно с точки зрения информационной безопасности, но нужда в данном функционале достаточно часто встречается в данных кейсах. 

- Системы, выполняющие высокопроизводительные расчеты на GPU - физические сервера т.к. виртуализация может негативно влиять на эффективность использования вычислительных ресурсов. В то же время следует отметить, что существуют [специализированные](https://www.researchgate.net/publication/287410240_GPGPU_Virtualization_System_Using_NVidia_Kepler_Series_GPU) решения паравиртуализации оптимизированные для применения в данной области.

## Задача 3

Выберите подходящую систему управления виртуализацией для предложенного сценария. Детально опишите ваш выбор.

Сценарии:

1. 100 виртуальных машин на базе Linux и Windows, общие задачи, нет особых требований. Преимущественно Windows based инфраструктура, требуется реализация программных балансировщиков нагрузки, репликации данных и автоматизированного механизма создания резервных копий.
2. Требуется наиболее производительное бесплатное open source решение для виртуализации небольшой (20-30 серверов) инфраструктуры на базе Linux и Windows виртуальных машин.
3. Необходимо бесплатное, максимально совместимое и производительное решение для виртуализации Windows инфраструктуры.
4. Необходимо рабочее окружение для тестирования программного продукта на нескольких дистрибутивах Linux.

### Ответ

1. VMWare ESXI или Hyper-V в данном случае обладают требуемым функционалом. Учитывая преимущественность Windows based инфраструктуры возможно Hyper-V подойдёт большей ввиду лучшей совместимости с оной.

2. Могут подойти Xen, KVM или Proxmox VE в зависимости от выполняемых операций, т.к. исходя из [бенчмарков](https://www.researchgate.net/publication/327482365_Performance_Evaluation_of_Xen_KVM_and_Proxmox_Hypervisors) их производительность сильно зависит от выполняемых задач. 

3. Microsoft Hyper-V Server имеющий наибольшую совместимость с Windows системами или как альтернатива Xen

4. Наибольшую скорость развёртывания в данном случае могут обеспечить решения виртуализации уровне ОС по типу [Docker](https://ao.ms/how-to-use-docker-to-test-any-linux-distribution-locally/)

## Задача 4

Опишите возможные проблемы и недостатки гетерогенной среды виртуализации (использования нескольких систем управления виртуализацией одновременно) и что необходимо сделать для минимизации этих рисков и проблем. Если бы у вас был выбор, то создавали бы вы гетерогенную среду или нет? Мотивируйте ваш ответ примерами.

### Ответ

Проблемы:

1. Увеличиваются требования к персоналу в части навыков эксплуатации разнородного ПО виртуализации

2. Увеличиваются операционные расходы в случае использования нескольких проприетарных систем управления виртуализацией

3. Усложняются операции по переносу виртуальных машин между различными средами и масштабированию

Необходимость создания гетерогенной среды может продиктовываться решаемыми задачами:

- необходимость распределения аппаратных ресурсов посредством полной виртуализации и облегчения задач размещения и развёртывания программного обеспечения посредством виртуализационных решений на уровне ОС;

- предоставления услуг IaaS и инфраструктурных облачных сервисов с диверсифицированной структурой виртуализационных платформ, в данном случае для облегчения управления используются программные комплексы по типу OpenStack, CloudStack, Eucalyptus.