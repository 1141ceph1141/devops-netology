# Домашнее задание к занятию "3.5. Файловые системы"

1. Узнайте о [sparse](https://ru.wikipedia.org/wiki/%D0%A0%D0%B0%D0%B7%D1%80%D0%B5%D0%B6%D1%91%D0%BD%D0%BD%D1%8B%D0%B9_%D1%84%D0%B0%D0%B9%D0%BB) (разряженных) файлах.

*Разрежённый файл (англ. sparse file) — файл, в котором последовательности нулевых байтов[1] заменены на информацию об этих последовательностях (список дыр).*

*Дыра (англ. hole) — последовательность нулевых байт внутри файла, не записанная на диск. Информация о дырах (смещение от начала файла в байтах и количество байт) хранится в метаданных ФС.2. Могут ли файлы, являющиеся жесткой ссылкой на один объект, иметь разные права доступа и владельца? Почему?*

1. Могут ли файлы, являющиеся жесткой ссылкой на один объект, иметь разные права доступа и владельца? Почему?

*Не могут, т.к. имеют общий индексный дескриптор*

![inode](.README_images/inode.png)

4. Сделайте `vagrant destroy` на имеющийся инстанс Ubuntu. Замените содержимое Vagrantfile следующим:
 
    ```bash
    Vagrant.configure("2") do |config|
      config.vm.box = "bento/ubuntu-20.04"
      config.vm.provider :virtualbox do |vb|
        lvm_experiments_disk0_path = "/tmp/lvm_experiments_disk0.vmdk"
        lvm_experiments_disk1_path = "/tmp/lvm_experiments_disk1.vmdk"
        vb.customize ['createmedium', '--filename', lvm_experiments_disk0_path, '--size', 2560]
        vb.customize ['createmedium', '--filename', lvm_experiments_disk1_path, '--size', 2560]
        vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', lvm_experiments_disk0_path]
        vb.customize ['storageattach', :id, '--storagectl', 'SATA Controller', '--port', 2, '--device', 0, '--type', 'hdd', '--medium', lvm_experiments_disk1_path]
      end
    end
    ```

    Данная конфигурация создаст новую виртуальную машину с двумя дополнительными неразмеченными дисками по 2.5 Гб.

![vagrant_new](.README_images/vagrant_new.png)

6. Используя `fdisk`, разбейте первый диск на 2 раздела: 2 Гб, оставшееся пространство.

![fdisk_new](.README_images/fdisk_new.png)

8. Используя `sfdisk`, перенесите данную таблицу разделов на второй диск.

![sfdisk](.README_images/sfdisk.png)

9. Соберите `mdadm` RAID1 на паре разделов 2 Гб.

![mdadm0](.README_images/mdadm0.png)

10. Соберите `mdadm` RAID0 на второй паре маленьких разделов.

![mdadm1](.README_images/mdadm1.png)

12. Создайте 2 независимых PV на получившихся md-устройствах.

![pvcreate](.README_images/pvcreate.png)

13. Создайте общую volume-group на этих двух PV.

![vgcreate](.README_images/vgcreate.png)

15. Создайте LV размером 100 Мб, указав его расположение на PV с RAID0.

![lvcreate](.README_images/lvcreate.png)

17. Создайте `mkfs.ext4` ФС на получившемся LV.

![mkfs](.README_images/mkfs.png)

19. Смонтируйте этот раздел в любую директорию, например, `/tmp/new`.

![mounted](.README_images/mounted.png)

21. Поместите туда тестовый файл, например `wget https://mirror.yandex.ru/ubuntu/ls-lR.gz -O /tmp/new/test.gz`.

![wget](.README_images/wget.png)

23. Прикрепите вывод `lsblk`.

![lsblk](.README_images/lsblk.png)

25. Протестируйте целостность файла:

     ```bash
     root@vagrant:~# gzip -t /tmp/new/test.gz
     root@vagrant:~# echo $?
     0
     ```
    
![test](.README_images/test.png)

26. Используя pvmove, переместите содержимое PV с RAID0 на RAID1.

![pvmove](.README_images/pmove.png)

28. Сделайте `--fail` на устройство в вашем RAID1 md.

![faulty](.README_images/faulty.png)

30. Подтвердите выводом `dmesg`, что RAID1 работает в деградированном состоянии.

![dmesg](.README_images/dmesg.png)

32. Протестируйте целостность файла, несмотря на "сбойный" диск он должен продолжать быть доступен:

     ```bash
     root@vagrant:~# gzip -t /tmp/new/test.gz
     root@vagrant:~# echo $?
     0
     ```
    
![test2](.README_images/test2.png)

33. Погасите тестовый хост, `vagrant destroy`.

![destroy](.README_images/destroy.png)