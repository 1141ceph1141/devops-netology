#!/usr/bin/env python3

import os

bash_command = ["cd $(git rev-parse --show-toplevel)", "git ls-files -m --full-name"]
result_os = os.popen(' && '.join(bash_command)).read()
for result in result_os.split('\n'):
    if result is not '':
        print(os.path.abspath(result))
