# Домашнее задание к занятию "3.6. Компьютерные сети, лекция 1"

1. Работа c HTTP через телнет.
- Подключитесь утилитой телнет к сайту stackoverflow.com
`telnet stackoverflow.com 80`
- отправьте HTTP запрос
```bash
GET /questions HTTP/1.0
HOST: stackoverflow.com
[press enter]
[press enter]
```
- В ответе укажите полученный HTTP код, что он означает?

![telnet](.README_images/telnet.png)

![301](.README_images/rfc_301_code.png)

*Код перенаправления "301 Moved Permanently" показывает, что запрошенный ресурс был окончательно перемещён в URL, указанный в заголовке Location.*

2. Повторите задание 1 в браузере, используя консоль разработчика F12.
- откройте вкладку `Network`
- отправьте запрос http://stackoverflow.com
- найдите первый ответ HTTP сервера, откройте вкладку `Headers`
- укажите в ответе полученный HTTP код.
- проверьте время загрузки страницы, какой запрос обрабатывался дольше всего?
- приложите скриншот консоли браузера в ответ.

![redirect](.README_images/307_redirect.png)

*HTTP код перенаправления  307 Temporary Redirect означает, что запрошенный ресурс был временно перемещён в URL-адрес, указанный в заголовке Location (en-US).*

![rfc_307_code](.README_images/rfc_307_code.png)

![time to load](.README_images/time to load.png)

*404 ms*

3. Какой IP адрес у вас в интернете?

![my_ip](.README_images/my_ip.png)

4. Какому провайдеру принадлежит ваш IP адрес? Какой автономной системе AS? Воспользуйтесь утилитой `whois`

![whois](.README_images/whois.png)

5. Через какие сети проходит пакет, отправленный с вашего компьютера на адрес 8.8.8.8? Через какие AS? Воспользуйтесь утилитой `traceroute`

![traceroute](.README_images/traceroute.png)

6. Повторите задание 5 в утилите `mtr`. На каком участке наибольшая задержка - delay?

*на участке между 5 и 6 хопом*

![mtr](.README_images/mtr.png)

7. Какие DNS сервера отвечают за доменное имя dns.google? Какие A записи? воспользуйтесь утилитой `dig`

![dig](.README_images/dns_google.png)

8. Проверьте PTR записи для IP адресов из задания 7. Какое доменное имя привязано к IP? воспользуйтесь утилитой `dig`

![PTR](.README_images/PTR.png)

*Привязано доменное имя `dns.google.`*

